//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  //agregar path para las peticiones json

var bodyParse = require('body-parser');
  app.use(bodyParse.json());
  app.use(function(req,rest, next){
    rest.header("Access-Control-Allow-Origin", "*");
    rest.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


var requestjson = require('request-json');
var path = require('path');
var urlMovimientos =  "https://api.mlab.com/api/1/databases/dbbanca3mb64172/collections/movimientos?apiKey=0ETBAE40mzcztLsqz1FQ02OF9LSbmFlF";
var urlUsuarios =  "https://api.mlab.com/api/1/databases/dbbanca3mb64172/collections/usuarios?apiKey=0ETBAE40mzcztLsqz1FQ02OF9LSbmFlF";
                    https://api.mlab.com/api/1/databases/my-db/collections/my-coll?q={"active": true}&apiKey=myAPIKey

var movimientosMLab = requestjson.createClient(urlMovimientos);
var usuariosMLab = requestjson.createClient(urlUsuarios);
var FILTRO ='&q=';
var SinPassword = '&f={"password": 0}';
var orderFechas = '&s={"fecha": -1, "hora": -1}';

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

// consulta de todas los datos de la consulta
app.get('/movimientos',function (req, res){

  movimientosMLab.get('', function(error, resM, body){
    console.log("error: " + error);
    console.log("resM: " + resM);
    console.log("body: " + body);

    if(error){
      console.log("error: " + error);
    }else{
      console.log("body: " + body);
      res.send(body);
    }

  });
});

//es para insertar un registro nuevo
app.post('/movimientos',function(req, res){
  movimientosMLab.post('',req.body, function(error, resM, body){
    console.log("req.body: " + req.body);
    console.log("error: " + error);
    console.log("resM: " + resM);
    console.log("body: " + body);
    if(error){
      console.log("error: " + error);
    }else{
      console.log("body: " + body);
      res.send(body);
    }
  });
});
///////////////////////////////////////////////////
//        Movimientos
///////////////////////////////////////////////////
// alta de Movimiento
app.post('/altaMovimiento',function(req, res){
  console.log("req.body: " + req.body);
  movimientosMLab.post('',req.body, function(error, resM, body){
    console.log("req.body: " + req.body);
    console.log("error: " + error);
    console.log("resM: " + resM.statusCode);
    console.log("body: " + body);
    if(error){
      console.log("error: " + error);
    }else{
      console.log("body: " + body);
      res.send(body);
    }
  });
});
///////////////////////////////////////
// Consulta de Movimientos
///////////////////////////////////////
app.get('/consultaMov/:cliente',function (req, resp){

  console.log("req.params.cliente: " + req.params.cliente);
  var query = {};
  query.user = req.params.cliente;
  var urlConsultaMovimientos = urlMovimientos + FILTRO + JSON.stringify(query)+orderFechas;
  console.log("urlConsultaMovimientos: " + urlConsultaMovimientos);
  var usuariosConsulta = requestjson.createClient(urlConsultaMovimientos);
  usuariosConsulta.get('', function(error, resM, body){
    console.log("error: " + error);
    console.log("resM : " + resM.statusCode);
    console.log("body : " + body);

    if(error){
      console.log("error: " + error);
      return error;
    }else{
      console.log("body.length...: " + body.length);

      if(body.length == 0) {
        resp.status(404).send("Sin Datos");
      }else {
        resp.status(200).send(body);
      }

    }
  });
});
///////////////////////////////////////////////////
//        usuarios
///////////////////////////////////////////////////
// alta de Clientes
app.post('/altaCliente',function(req, res){
  console.log("req.body: " + req.body);
  usuariosMLab.post('',req.body, function(error, resM, body){
    console.log("req.body: " + req.body);
    console.log("error: " + error);
    console.log("resM: " + resM.statusCode);
    console.log("body: " + body);
    if(error){
      console.log("error: " + error);
    }else{
      console.log("body: " + body);
      res.status(resM.statusCode).send(body);
    }
  });
});
///////////////////////////////////////
// Consulta de Login
///////////////////////////////////////
app.get('/validaCliente',function (req, resp){

  console.log("query.user: " + req.query.user);
  console.log("query.pwd : " + req.query.password);
  var query = {};
  query.user = req.query.user;
  query.password = req.query.password;
  var urlConsultaUsuario = urlUsuarios + FILTRO + JSON.stringify(query) + SinPassword;
  console.log("urlConsultaUsuario: " + urlConsultaUsuario);
  var usuariosConsulta = requestjson.createClient(urlConsultaUsuario);
  usuariosConsulta.get('', function(error, resM, body){
    console.log("error: " + error);
    console.log("resM : " + resM.statusCode);
    console.log("body : " + body);

    if(error){
      console.log("error: " + error);
      return error;
    }else{
      console.log("body.length...: " + body.length);

      if(body.length == 0) {
        resp.status(404).send("Sin Datos");
      }else {
        resp.status(200).send(body);
      }

    }
  });
});

var consultaCliente=function(cliente){
  console.log("********************************");
  console.log("cliente: " + cliente);
  var query = {};
  query.user = cliente;
}

/*

      for (var indice in body) {
        console.log("En el índice '" + indice + "' hay este valor: " + body[indice]);
        console.log("En el índice '" + indice + "' hay este valor: " + body[indice].user);
      }
      console.log("body.length...: " + body.length);

      body.forEach(function(value){
        console.log("value");
        console.log(value);
      });
      resp.status(200).send(body)
    }*/


//regresa todos los datos
app.get('/clientes',function (req, res){
  res.sendFile(path.join(__dirname,'clientes.json'));
});
//regresa uno en particular
app.get('/clientes/:idClientes',function(req, res){
  res.send('Aquí tiene el Cliente númnero: ' + req.params.idClientes)
});
app.post('/clientes',function(req, res){
  res.send('Alta correcta de Clientes para el log de mensajes')
});


app.delete('/clientes',function(req, res){
  res.send('Borrado correcta de Clientes')
});

app.put('/clientes',function(req, res){
  res.send('Modificación correcta de Clientes')
});
