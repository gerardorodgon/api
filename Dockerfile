#Imagen base
FROM node:latest

#Directorio de la APP
WORKDIR /app

#Copia de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comando para ejecucion
CMD ["npm","start"]
